﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_26._3___Land
{
    internal class Land
    {
        private string _hoofdstad;
        private string _naam;

        public Land() { }
        public Land(string naam, string hoofdstad)
        {
            this.Naam = naam;
            this.Hoofdstad = hoofdstad;
        }

        public string Naam
        {
            get { return _hoofdstad; }
            set
            {
                if (!String.IsNullOrEmpty(value)) { _hoofdstad = value; } else { throw new FormatException("U heeft geen hoofdstad ingegeven."); }
            }
        }
        public string Hoofdstad
        {
            get { return _naam; }
            set
            {
                if (!String.IsNullOrEmpty(value)) { _naam = value; } else { throw new FormatException("U heeft geen landcode ingegeven."); }
            }
        }

        public override string ToString()
        {
            return $"Hashcode: {this.GetHashCode()} || {Naam} - {Hoofdstad} ";
        }
        public override bool Equals(object? obj)
        {
           bool resultaat = false;
            //3 dingen doen:
            //1. Controleren of mijn object niet gelijk is aan null. COPY PASTE
            if (obj != null)
            {
                //2. Controleren of het datatype gelijk is aan elkaar. COPY PASTE
                if (GetType() == obj.GetType())
                {
                    //3. Eigen validatie
                    Land land = (Land)obj;
                    if (this.Hoofdstad == land.Hoofdstad)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }
    }
}
