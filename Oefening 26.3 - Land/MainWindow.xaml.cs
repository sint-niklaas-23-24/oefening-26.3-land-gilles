﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Oefening_26._3___Land
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        List<Land> lijstLanden = new List<Land>();
        Land land;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtResultaat.IsEnabled = false;
            txtResultaat.Text = string.Empty;
            rdoMonarchie.IsChecked = true;
        }

        private void btnAddLand_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!(txtLandNaam.Text.Any(Char.IsDigit) || txtHoofdstad.Text.Any(Char.IsDigit) || txtStaatshoofd.Text.Any(Char.IsDigit)))
                {
                    if (rdoMonarchie.IsChecked == true)
                    {
                        land = new Monarchie(txtLandNaam.Text, txtHoofdstad.Text, txtStaatshoofd.Text);
                    }
                    else if (rdoRepubliek.IsChecked == true)
                    {
                        land = new Republiek(txtLandNaam.Text, txtHoofdstad.Text, txtStaatshoofd.Text);
                    }
                    else if (rdoOverige.IsChecked == true)
                    {
                        land = new Land(txtLandNaam.Text, txtHoofdstad.Text);
                    }
                    if (lijstLanden.Contains(land) == true)
                    {
                        throw new FormatException("U mag niet 2 keer hetzelfde land aanmaken.");
                    }
                    lijstLanden.Add(land);
                    txtResultaat.Text += land.ToString() + Environment.NewLine;
                    TXTSLeegmaken();
                }
                else
                {
                    throw new FormatException("Namen van landen, steden of personen mogen geen numerieke waardes bevatten");
                }
            }
            catch (FormatException fe)
            {
                MessageBox.Show(fe.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Er ging iets mis", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void rdoRepubliek_Checked(object sender, RoutedEventArgs e)
        {
            TXTSLeegmaken();
            txtStaatshoofd.IsEnabled = true;
        }

        private void rdoMonarchie_Checked(object sender, RoutedEventArgs e)
        {
            TXTSLeegmaken();
            txtStaatshoofd.IsEnabled = true;
        }
        private void rdoOverige_Checked(object sender, RoutedEventArgs e)
        {
            TXTSLeegmaken();
            txtStaatshoofd.IsEnabled = false;
        }
        private void TXTSLeegmaken()
        {
            txtLandNaam.Text = string.Empty;
            txtHoofdstad.Text = string.Empty;
            txtStaatshoofd.Text = string.Empty;
        }
    }
}
