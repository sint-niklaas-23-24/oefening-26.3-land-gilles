﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_26._3___Land
{
    internal class Monarchie : Land
    {
        private string _koning;

        public Monarchie(string naam, string hoofdstad, string koning) : base(naam, hoofdstad) 
        { 
            this.Koning = koning;
        }

        public string Koning
        {
            get { return _koning; }
            set
            {
                if (!String.IsNullOrEmpty(value)) { _koning = value; } else { throw new FormatException("U heeft geen koning(in) ingegeven."); }
            }
        }

        public override string ToString()
        {
            return base.ToString() + $"(Koning(in): {Koning})";
        }
    }
}
