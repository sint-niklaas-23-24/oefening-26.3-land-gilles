﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_26._3___Land
{
    internal class Republiek : Land
    {
        private string _president;

        public Republiek(string naam, string hoofdstad, string president) : base(naam, hoofdstad) 
        { 
            this.President = president;
        }

        public string President
        {
            get { return _president; }
            set { if (!String.IsNullOrEmpty(value)) { _president = value; } else { throw new FormatException("U heeft geen president ingegeven."); } }
        }

        public override string ToString()
        {
            return base.ToString() + $"(President: {President})";
        }
    }
}
